# GIT Cheat Sheet

| **Comandi** | **Spiegazione** |
| --- | --- |
| *git init* | git initialize; inizializza una repo |
| *git status*| visualizza lo stato del progetto|
|*git config user.name* "(username)"|per configurare lo username dell'utente|
|*git config user.email* "(mail)"|per configurare la mail dell'utente|
||*git config --global* per rendere le configurazioni global e inserirle in $HOME/.gitconfig|
|*git add*|aggiungere un file allo staged (insieme di cambiamenti che si vuole aggiungere al progetto)|
||*git add .* per aggiungere l'intero progetto|
|*git commit -m* "(messaggio)"|effettuare le modifiche tramite un commit|
|Stati del codice:|untracked -> staged -> committed|
|*git log*|mostra tutti i commit effettuati, con messaggi allegati|
||*git log --oneline* per ridurre informazioni|
|*git rm*|rimuovi un file (modifica già inserita in staged)|
|*git restore --staged* (nome del file)|per annullare uno staging|
|*git rm --cached* (nome del file)|toglie un file da Git mantenendolo nel computer|
|*mv* (origine) (destinazione)|per spostare (o rinominare) un file|
||*git mv*: più vantaggioso per la logica euristica di Git|
|*git show* (hash)|mostra nel dettaglio il commit identificato da quell'hash|
|*git diff* (hash)|mostra esattamente le differenze del commit indicato rispetto a quello prima|
||*git diff* per vedere solo le modifiche dall'ultimo commit|
|*git checkout*|aggiorna i file dell'ambiente alla loro versione in un tree specifico|
|più nel dettaglio:|*git checkout*(hash) riporta l'ambiente a com'era in quel commit|
||*git checkout -b* (nome del branch) per creare un branch|
||*git checkout* (nome del file) annulla tutte le modifiche fatte a quel file dall'ultimo commit|
||*git checkout* (nome del branch) per cambiare branch di lavoro|
|*git branch*|per vedere la lista dei branch|
|*git merge* (nome del branch)|nel master, per unire il branch scelto|
|in caso di conflitti nel merge:|*git merge --abort* per abortire tutti i merge|
||*git merge --continue* per andare avanti con le modifiche|
|*git rebase* (nome del branch)|rimette in pari il branch con il main|
|*git push* (dove) (da dove)|manda tutti i nuovi commit ad un'altra repo|
|*git push -u*|per automatizzare ogni push successivo|
|*git remote -v*|mostra l'origine di ciò che è stato pullato dalla repo di pull|
|*git pull*|scarica i commit dall'origin|
|*git reset* (nome del branch da copiare)|reset del branch al branch menzionato|
|*git commit --amend*|prende tutto ciò che c'è in area di staging e lo aggiunge all'ultimo commit (comando da usare con cautela)|
|*git add -p* / *git add -i*|modalità interattiva dell'add, consente operazioni cambiamento per cambiamento|
|*git reset HEAD~*(n)| cancella dalla storia gli ultimi n commit, mantenendone i cambiamenti
|*git revert*|inverte i cambiamenti introdotti da un commit e aggiunge un commit automatico coon i cambiamenti segnalati|
|*git stash*|mette da parte i cambiamenti non committati|
|*git cherry-pick* (commit)|prende un commit particolare e lo aggiunge al proprio branch|
|*git blame*|mostra chi ha modificato e in che modo qualcosa|
|*git tag* (nome del tag)|mette un'etichetta ad un commit, segnando un punto nella storia del codice, generalmente seguendo il versionamento semantico|

